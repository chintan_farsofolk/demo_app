import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Auth, User, UserDetails, IDetailedError } from '@ionic/cloud-angular';


@Injectable()
export class AuthService {
  loggedInUser: User;
  constructor(public http: Http, public auth: Auth, public user: User) {

  }

  public login(credentials) {
    if (credentials.email === null || credentials.password === null) {
      return Observable.throw("Please insert credentials");
    } else {
      return Observable.create(observer => {

        let details: UserDetails = {
          'email': credentials.email,
          'password': credentials.password
        };
        this.auth.login('basic', details).then(() => {
          console.log(this.user);
          observer.next(true);
          observer.complete();
        }).catch(error => {
          observer.next(false);
          observer.complete();
          console.log("in error ", error);
        });

      });
    }
  }

  public register(credentials) {
    if (credentials.email === null || credentials.password === null) {
      return Observable.throw("Please insert credentials");
    } else {
      return Observable.create(observer => {

        let details: UserDetails = {
          'email': credentials.email,
          'password': credentials.password
        };

        this.auth.signup(details).then(() => {
          console.log(this.user);
          observer.next(true);
          observer.complete();
        }, (err: IDetailedError<string[]>) => {

          for (let error of err.details) {

            if (error === 'required_email') {
              // email missing
            } else if (error === 'required_password') {
              // password missing
            } else if (error === 'conflict_email') {
              // email already in use
            } else if (error === 'conflict_username') {
              // username alerady in use
            } else if (error === ' invalid_email') {
              // email not valid
            }

          }
          observer.next(false);
          observer.complete();

        });
      });
    }
  }

  public logout() {
    return Observable.create(observer => {
      this.auth.logout();
      observer.next(true);
      observer.complete();
    });
  }

  public isLoggedIn() {
    return this.auth.isAuthenticated();
  }

}