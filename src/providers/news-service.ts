import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the FirstService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class NewsService {
  public news:any;
  //public news_id:any;
  constructor(public http: Http) {
    console.log('Hello NewsService Provider');
  }

 getNews() {


        var url = 'http://new.anasource.com/team12/team5/test/news_listing.json';
        var response = this.http.get(url).map(res => res.json());
        
       //return this.news;
       return response;
    }
 /*getNewsDetail(news_id) {

      console.log("In news detail");
        
        var response = this.http.get(url).map(res => res.json());
        
       //return this.news;
       //console.log("response",response);
       return response;
    } 
*/
getNewsDetail(news_id){

     if (this.news) {
    // already loaded data
      return Promise.resolve(this.news);
    }

     return new Promise(resolve => {
    // We're using Angular HTTP provider to request the data,
    // then on the response, it'll map the JSON data to a parsed JS object.
    // Next, we process the data and resolve the promise with the new data.

    var url = 'http://new.anasource.com/team12/team5/test/news_details.json';
    this.http.get(url)
      .map(res => res.json())
      .subscribe(data => {
        // we've got back the raw data, now generate the core schedule data
        // and save the data for later reference
        this.news = data;
        resolve(this.news);
      });
  });
}
  
  
  
  
}
