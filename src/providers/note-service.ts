import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class NoteService {

  constructor(public http: Http) {
  }

  saveToserver(data: any) {
    let body = JSON.stringify({ "data": data });
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ "headers": headers });
    //console.log("data  ",body,options);
    var url = 'http://new.anasource.com/team12/team5/test/';
    var response = this.http.post(url, body, options).map(res => res.json());
    return response;
  }

  getNotes(email: string){
    let body = JSON.stringify({ "data": {"email": email,"action":"get"} });
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ "headers": headers });
    var url = 'http://new.anasource.com/team12/team5/test/';
    var response = this.http.post(url, body, options).map(res => res.json());
    return response;
  }

}
