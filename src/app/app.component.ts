import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { Categories } from '../pages/categories/categories';
import { News } from '../pages/news_list/news_list';
import { Notes } from '../pages/note/note';
import { LoginPage } from '../pages/login/login';
import { AuthService } from '../providers/auth-service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = Notes;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, private auth: AuthService) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      
      { title: 'Cats', component: Categories },
      { title: 'News', component: News },
      { title: 'Notes', component: Notes }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  logout(){
    this.auth.logout().subscribe(succ => {
        //this.nav.setRoot(LoginPage)
        //this.nav.pop();
    });
  }
  login(){
    this.nav.push(LoginPage);
  }
}
