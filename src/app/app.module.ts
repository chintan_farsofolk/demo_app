import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { CloudSettings, CloudModule } from '@ionic/cloud-angular';
import { MyApp } from './app.component';
import { Categories } from '../pages/categories/categories';
import { News } from '../pages/news_list/news_list';
import {NewsDetailPage} from '../pages/news_detail/news_detail';
import { Notes } from '../pages/note/note';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { AuthService } from '../providers/auth-service';

const cloudSettings: CloudSettings = {
  'core': {
    'app_id': '45c33b43'
  }
};

@NgModule({
  declarations: [
    MyApp,
    Categories,
    News,
    NewsDetailPage,
    Notes,
    LoginPage,
    RegisterPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    CloudModule.forRoot(cloudSettings)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Categories,
    News,
    NewsDetailPage,
    Notes,
    LoginPage,
    RegisterPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler},AuthService]
})
export class AppModule {}

