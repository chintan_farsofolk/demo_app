import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { AuthService } from '../../providers/auth-service';
import { NoteService } from '../../providers/note-service';
import { User } from '@ionic/cloud-angular';

@Component({
  templateUrl: 'note.html',
  selector: "note-item-list",
  providers: [Storage, NoteService]
})
export class Notes {
  public notes: any;
  public note: any;
  //private local;
  constructor(public navCtrl: NavController, private auth: AuthService, public storage: Storage, public user: User, public noteService: NoteService) {
    this.notes = [];
    this.note = {};
    this.loadNotes();
    //console.log("call constructor", this.notes);
  }

  loadNotes() {
    if (this.auth.isLoggedIn()) {
      //this.notes = this.user.get('notes');
      this.noteService.getNotes(this.auth.user.details.email).subscribe(data => {
        console.log("Data return from server ", data.notes);
        if (data.status != 'failed') {
          let tempData = JSON.parse(data.notes);
          console.log(tempData);
          if(tempData != null){
            this.notes = tempData;
          }
          
        }
      });
    } else {
      this.storage.get('notes').then((value: any) => {
        if (value != null) {
          this.notes = value;
        }
        console.log('My value is:', value);
      });
    }

  }
  addItem(noteText: string) {
    if (noteText !== null && noteText != undefined && noteText.length > 0) {
      this.notes.push({ "title": noteText });
      this.storage.set('notes', this.notes);
      this.note.title = "";
    }
  }
  deleteItem(i: any) {
    this.notes.splice(i, 1);
  }
  deleteFromStorage() {
    this.storage.set('notes', this.notes);
    alert("Record has been deleted");
  }

  updateToServer() {
    let noteData = { "email": this.auth.user.details.email, "action": "save", "notes": this.notes };
    this.noteService.saveToserver(noteData).subscribe(data => {
      console.log("Data return from server ", data);
      if(data.status == 'success'){
        alert("Record has been updated");
      }
    });
  }

}