import { Component } from '@angular/core';



import {FirstService} from '../../providers/first-service';

@Component({
  templateUrl: 'categories.html',
  providers: [FirstService]
})
export class Categories {
   public cats: any;

  constructor(public firstService: FirstService){
    this.loadCats();
  }

  loadCats(){
    this.firstService.load()
    .then(data => {
      //console.log("Data",data);
      this.cats = data.categories;
    });
  }
}
