import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import {NewsService} from '../../providers/news-service';
import {NewsDetailPage} from '../news_detail/news_detail';


@Component({
  templateUrl: 'news_list.html',
  selector :"news-item-list",
  providers: [NewsService]
})
export class News {
   public news: any;

  constructor(public newsService: NewsService,public navCtrl: NavController){
    this.loadNews();
  }

  loadNews(){
    this.newsService.getNews().subscribe(data => {
      //console.log("Data",data);
      this.news = data.news;
    });
  }
  openItem(id) {
      console.log("In open Item"+id);
    this.navCtrl.push(NewsDetailPage, {
      id: id
    });
  }
}
