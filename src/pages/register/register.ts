import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController, Loading } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';


@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {
  createSuccess = false;
  credentials = { email: '', password: '' };
  loading: Loading;

  constructor(public navCtrl: NavController, private auth: AuthService, private alertCtrl: AlertController, private loadingCtrl: LoadingController) { }

  public register() {
    this.showLoading();
    this.auth.register(this.credentials).subscribe(success => {
      if (success) {
        this.loading.dismiss();
        this.createSuccess = true;
        this.showPopup("Success", "Account created.");
      } else {
        this.loading.dismiss();
        this.showPopup("Error", "Problem creating account.");
      }
    },
      error => {
        this.loading.dismiss();
        this.showPopup("Error", error);
      });
  }


  showPopup(title, text) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: [
        {
          text: 'OK',
          handler: data => {
            if (this.createSuccess) {
              this.navCtrl.popToRoot();
            }
          }
        }
      ]
    });
    alert.present();
  }


  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }


}
