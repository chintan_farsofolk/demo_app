import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import {NewsService} from '../../providers/news-service';
import {NewsDetail} from '../../model/news-detail';


@Component({
  selector: 'page-item-detail',
  templateUrl: 'news_detail.html',
  providers: [NewsService,NewsDetail]
})
export class NewsDetailPage {
  id: any;
//public news_detail : any;
  constructor(public navCtrl: NavController, navParams: NavParams,public newsService: NewsService, public news_detail: NewsDetail) {
    this.id = navParams.get('id') ;
    //this.news_detail = {"id":"",title:"","detail":""};
    console.log("news_detail constructure "+this.id);
    this.loadNewsDetail();
  }



  loadNewsDetail(){
    this.newsService.getNewsDetail(this.id).then(data => {
      

      for (var nw in data.news){

        console.log("NW = "+nw);
        
        if(data.news[nw].id == this.id){
          this.news_detail = data.news[nw];
          break;
        }
      }
      console.log("this.news = ",this.news_detail);
    });
  }

}
